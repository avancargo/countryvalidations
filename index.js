// @ts-check

/**
 *
 * @param {String} country - pais
 * @returns {String} - identificador de impuesto, devuelve 'Invalid' si el pais no existe o se envio mal el parametro
 */
const getCountryTaxIdentifier = (country) => {
  switch (country) {
    case 'AR':
      return 'CUIT'
    case 'CL':
      return 'RUT'
    case 'UY':
      return 'RUT'
    default:
      return 'Invalid'
  }
}

/**
 *
 * @param {String} country - pais
 * @returns {Object} - objeto con dos campos, mobile y web, dentro estan las mascaras para cada uno
 */

const getCountryTaxIdentifierMask = (country) => {
  switch (country) {
    case 'AR':
      return {
        mobile: '[00]-[00000000]-[0]',
        web: {
          numericOnly: true,
          blocks: [2, 8, 1],
          delimiter: '-'
        }
      }
    case 'CL':
      return {
        mobile: '[00].[000].[000]-[0]',
        web: {
          blocks: [2, 3, 3, 1],
          delimiters: ['.', '.', '-']
        }
      }
    case 'UY':
      return {
        mobile: '[00].[000000].[000]-[0]',
        web: {
          numericOnly: true,
          blocks: [2, 6, 3, 1],
          delimiters: ['.', '.', '-']
        }
      }
    default:
      return 'Invalid'
  }
}

/**
 *
 * @param {String} country - pais
 * @returns {Number|String} - Largo del identificador de impuesto, devuelve 'Invalid' si el pais no existe o se envio mal el parametro
 */

const getCountryTaxIdentifierLength = (country) => {
  switch (country) {
    case 'AR':
      return 11
    case 'CL':
      return 9
    case 'UY':
      return 12
    default:
      return 'Invalid'
  }
}
/**
 * @param {string} vehicleRegistration
 * @param {string[]} plates
 */
function validateVehicleRegistration (vehicleRegistration, plates) {
  const res = vehicleRegistration.split('').map(x => {
    if (x.match(/[aA-zZ]/i)) {
      return 'A'
    } else {
      return '9'
    }
  }
  ).join('')

  return plates.includes(res)
}
/**
 *
 * @param {String} country - pais
 * @param {String} plate - patente a validar
 * @returns {Boolean|String} - devuelve si la matricula es valida o no, devuelve Invalid si el pais no existe o se envio mal el parametro
 */
const validateCountryPlateFormat = (country, plate) => {
  switch (country) {
    case 'AR':
      return validateVehicleRegistration(plate, ['AAA999', 'AA999AA'])
    case 'CL':
      return validateVehicleRegistration(plate, ['AA9999', 'AAAA99'])
    case 'UY':
      return validateVehicleRegistration(plate, ['AAA9999', 'A999999'])
    default:
      return 'Invalid'
  }
}
/**
 *
 * @param {String} country - pais
 * @returns {String} - Nombre del DNI por pais, devuelve 'Invalid' si el pais no existe o se envio mal el parametro
 */
const getCountryDocumentIdentifier = (country) => {
  switch (country) {
    case 'AR':
      return 'DNI'
    case 'CL':
      return 'CI'
    case 'UY':
      return 'CI'
    default:
      return 'Invalid'
  }
}
/**
 *
 * @param {String} country - pais
 * @returns {Number|String} - Cantidad de digitos del DNI, devuelve 'Invalid' si el pais no existe o se envio mal el parametro
 */
const getCountryDocumentIdentifierLenght = (country) => {
  switch (country) {
    case 'AR':
      return 8
    case 'CL':
      return 9
    case 'UY':
      return 8
    default:
      return 'Invalid'
  }
}

/**
 *
 * @param {String} number - Numero del identificador de impuestos a formatear
 * @param {String} country - pais
 * @returns {String} - Numero formateado con - o . depende corresponda, devuelve 'Invalid' si el pais no existe o se envio mal el parametro
 */
const formatCountryTaxIdentifier = (number, country) => {
  switch (country) {
    case 'AR':
      return number.substring(0, 2) + '-' + number.substring(2, 10) + '-' + number.substring(10, 11)
    case 'CL':
      return number.substring(0, 2) + '.' + number.substring(2, 5) + '.' + number.substring(5, 8) + '-' + number.substring(8, 9)
    case 'UY':
      return number.substring(0, 2) + '.' + number.substring(2, 8) + '.' + number.substring(8, 11) + '-' + number.substring(11, 12)
    default:
      return 'Invalid'
  }
}

/**
 *
 * @param {String} email - Correo electronico a validar
 * @returns {Boolean} - Retorna treu si el email es valido
 */
const validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

module.exports = {
  getCountryTaxIdentifier,
  getCountryTaxIdentifierMask,
  getCountryTaxIdentifierLength,
  validateCountryPlateFormat,
  getCountryDocumentIdentifier,
  getCountryDocumentIdentifierLenght,
  formatCountryTaxIdentifier,
  validateEmail
}
